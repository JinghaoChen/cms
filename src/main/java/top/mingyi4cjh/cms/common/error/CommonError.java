package top.mingyi4cjh.cms.common.error;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:37
 */
public interface CommonError {
    /**
     * 获取当前实例的错误码。
     *
     * @return errorCode
     */
    int getErrCode();

    /**
     * 获取当前实例的错误信息。
     *
     * @return errorMessage
     */
    String getErrMsg();

    /**
     * 给当前示例的错误码设定一个新的错误信息，并获取新的CommonError实例。
     * 这个示例将不会被存储，可用于如10001号BusinessError，将其参数不合法信息更具体化。
     *
     * @param errMsg
     * @return CommonError instance
     */
    CommonError setErrMsg(String errMsg);
}

