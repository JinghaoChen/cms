package top.mingyi4cjh.cms.common.error;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:39
 */
public enum EmBusinessError implements CommonError {
    /**
     * 第10001号BusinessError
     * 当入参不符合要求时，将生成这个BusinessError以返回给前端。
     */
    PARAMETER_VALIDATION_ERROR(10001, "参数不合法"),
    /**
     * 第10002号BusinessError
     * 当系统发生未定义的错误时，将生成这个BusinessError返回给前端。
     */
    UNKNOWN_ERROR(10002, "未知错误"),

    /**
     * 第20001号BusinessError
     * 当当前处理的用户不存在时，将生成这个BusinessError以返回给前端。
     */
    USER_NOT_EXIST(20001, "用户不存在"),
    /**
     * 第20002号BusinessError
     * 当用户输入的用户名或密码错误时（无法匹配时），将生成这个BusinessError以返回给前端。
     */
    USER_LOGIN_FAIL(20002, "用户用户名或密码错误"),
    /**
     * 第20003号BusinessError
     * 当用户想要注册的用户名已经被注册过时，将会生成这个BusinessError以返回给前端。
     */
    USER_ALREADY_EXIST(20003, "用户已存在"),

    /**
     * 第20004号BusinessError
     * 当用户注册使用的邮箱已经被使用过，则会生成这个BusinessError以返回给前端。
     */
    EMAIL_ALREADY_USED(20004, "邮箱已被使用"),

    /**
     * 第20005号BusinessError
     * 当用户找回密码是，输入的userName和email无法匹配时将返回该BusinessError。
     */
    EMAIL_CHECK_FAIL(20005, "用户名或邮箱错误"),

    /**
     * 第20006号BusinessError
     * 当用户身份未验证却尝试重置密码时，将返回改BusinessError。
     */
    AUTHENTICATION_NOT_VERIFIED(20006, "用户身份未验证"),

    /**
     * 第20007号BusinessError
     * 当用户注册使用的手机号已经被使用过，则会生成这个BusinessError以返回给前端。
     */
    PHONE_ALREADY_USED(20007, "手机号已被使用"),

    /**
     * 第20008号BusinessError
     * 当用户操作的数据并非本人的数据时将抛出该异常返还给前端。
     */
    ABNORMAL_OPERATION_DATA(20008, "操作数据异常"),

    /**
     * 第20009号BusinessError
     * 当用户重复登录时将抛出该异常。
     */
    USER_ALREADY_LOGIN(20009, "用户已登录"),

    /**
     * 第21001号BusinessError
     * token签名无效时抛出。
     */
    INVALID_SIGNATURE(21001, "无效身份签名"),

    /**
     * 第21002号BusinessError
     * token过期时抛出
     */
    TOKEN_EXPIRED(21002, "登录信息过期"),

    /**
     * 第21003号BusinessError
     * token算法不一致时抛出
     */
    INCONSISTENT_ALGORITHM(21003, "无效token算法"),

    /**
     * 第21004号BusinessError
     * token无效时抛出
     */
    INVALID_TOKEN(21004, "无效身份信息"),

    /**
     * 第20010号BusinessError
     * 当用户无权操作时将抛出该异常。
     */
    UNAUTHORIZED_OPERATION(20010, "无权操作"),

    /**
     * 第30001号BusinessError
     * 当用户给出的邮件格式错误时，将生成该BusinessError返回给前端。
     */
    MAIL_FORMAT_ERROR(30001, "邮件格式错误"),

    /**
     * 第30002号BusinessError
     * 当用户给的验证码与邮箱无法匹配时，将生成该BusinessError返回给前端。
     */
    OTP_CODE_ERROR(30002, "验证码错误"),

    /**
     * 第30003号BusinessError
     * 当用户请求otpCode过于频繁时，将产生该BusinessError返回给前端。
     */
    SEND_FREQUENTLY_ERROR(30003, "请求OTP过于频繁"),

    /**
     * 第40001号BusinessError
     * 当文件转换发送IOException时将转化为该异常返回给前端。
     */
    FILE_CONVERT_FAIL(40001, "文件转换失败"),

    /**
     * 第40002号BusinessError
     * 当用户身份不符合时，将抛出该异常给前端。
     */
    FILE_OWNER_FAIL(40002, "您无权下载该文件"),

    /**
     * 第40003号BusinessError
     * 当文件不存在时，将抛出该异常给前端。
     */
    FILE_NOT_EXIST(40003, "文件不存在"),

    /**
     * 第40004号BusinessError
     * 当文件类型不支持时，将抛出该异常给前端。
     */
    UNSUPPORTED_FILE_TYPE(40004, "文件类型不支持"),

    /**
     * 第40005号BusinessError
     * 当文件上传失败时，将抛出该异常给前端。
     */
    FILE_UPLOAD_FAIL(40005, "文件上传失败"),

    /**
     * 第40006号BusinessError
     * 当文件不支持预览时，将抛出该异常给前端。
     */
    FILE_CAN_NOT_PREVIEW(40006, "文件不支持预览"),

    /**
     * 第40007号BusinessError
     * 当文件预览抛出异常时，将抛出该异常给前端。
     */
    FILE_PREVIEW_FAIL(40007, "文件预览失败"),

    /**
     * 第50001号BusinessError
     * 当用户希望操作不是自己的文件时，将抛出该异常给前端。
     */
    ARTICLE_OWNER_FAIL(50001, "您无权对该文件操作"),

    /**
     * 第90001号BusinessError
     * 从数据库中查询数据集为空时抛出
     */
    GETTING_INFORMATION_ERROR(90001, "获取信息错误"),

    /**
     * 第90004号BusinessError
     * 抛出IOException时转换成该异常
     */
    FILE_PROCESS_ERROR(90004, "文件处理错误"),

    /**
     * 第90005号BusinessError
     * 访问过于频繁时抛出
     */
    ACCESS_FREQUENTLY(90005, "访问过于频繁"),

    ;

    EmBusinessError(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    private int errCode;
    private String errMsg;

    @Override
    public int getErrCode() {
        return errCode;
    }

    @Override
    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }
}

