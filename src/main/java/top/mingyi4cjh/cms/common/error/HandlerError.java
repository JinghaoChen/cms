package top.mingyi4cjh.cms.common.error;

/**
 * @author MingYi
 */
public class HandlerError{
    private int errCode;

    private String errMsg;

    public HandlerError(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public HandlerError(CommonError commonError) {
        this.errCode = commonError.getErrCode();
        this.errMsg = commonError.getErrMsg();
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
