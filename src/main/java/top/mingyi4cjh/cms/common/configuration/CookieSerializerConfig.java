package top.mingyi4cjh.cms.common.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:44
 */
@Configuration
public class CookieSerializerConfig{
    /**
     * 在主域中储存Cookie，子域中共享Cookie
     */
    @Bean
    public CookieSerializer cookieSerializer() {

        // 默认 Cookie 序列化
        DefaultCookieSerializer defaultCookieSerializer = new DefaultCookieSerializer();

        // Cookie名字，默认为 SESSION
        defaultCookieSerializer.setCookieName("SESSION");

        // 域，这允许跨子域共享cookie，默认设置是使用当前域。
        defaultCookieSerializer.setDomainName("riyun.xyz");

        // Cookie的路径。
        defaultCookieSerializer.setCookiePath("/");

        return defaultCookieSerializer;
    }
}

