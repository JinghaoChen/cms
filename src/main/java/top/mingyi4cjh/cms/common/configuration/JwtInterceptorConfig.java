package top.mingyi4cjh.cms.common.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.mingyi4cjh.cms.common.interceptor.JwtInterceptor;

/**
 * @author MingYi
 */
@Configuration
public class JwtInterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加拦截器
        registry.addInterceptor(new JwtInterceptor())
                //要保护的接口，即需要验证的接口
                .addPathPatterns("/article/**", "/file/**")
                //用户下的接口进行放行,要不无法获取token.
                .excludePathPatterns("/user/**");
    }
}