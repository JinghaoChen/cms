package top.mingyi4cjh.cms.common.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.mingyi4cjh.cms.common.interceptor.AccessLimitInterceptor;

import javax.annotation.Resource;

/**
 * @author MingYi
 */
@Configuration
public class AccessLimitConfig implements WebMvcConfigurer {

    @Resource
    public AccessLimitInterceptor accessLimitInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(accessLimitInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/user/login", "/user/registerByEmail", "/user/getEmailOtp");
    }
}
