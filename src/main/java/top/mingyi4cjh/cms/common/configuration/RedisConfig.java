package top.mingyi4cjh.cms.common.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:41
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 3600)
@EnableCaching
public class RedisConfig {
}
