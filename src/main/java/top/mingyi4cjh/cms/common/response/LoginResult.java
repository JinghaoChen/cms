package top.mingyi4cjh.cms.common.response;

/**
 * @author MingYi
 */
public class LoginResult {
    /**
     * 操作状态
     */
    private String status;

    /**
     * 用户数据信息
     */
    private Object data;

    /**
     * token
     */
    private String token;

    public static LoginResult create(Object data, String token) {
        return LoginResult.create(data, token, "success");
    }

    public static LoginResult create(Object data, String token, String status) {
        LoginResult loginResult = new LoginResult();
        loginResult.setData(data);
        loginResult.setToken(token);
        loginResult.setStatus(status);
        return loginResult;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
