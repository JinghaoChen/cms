package top.mingyi4cjh.cms.common.utils;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:48
 */
public enum UuidManager {
    /**
     * uuid的管理者，用于维护一个雪花id生成器
     */
    UUID_MANAGER,
    ;

    private final SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker(1, 1);

    private UuidManager() {
    }

    public Long getUuid() {
        return snowflakeIdWorker.nextId();
    }

}

