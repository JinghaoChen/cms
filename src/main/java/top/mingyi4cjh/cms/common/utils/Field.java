package top.mingyi4cjh.cms.common.utils;

import java.io.File;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:51
 */
public class Field {
    /**
     * 登录的用户id
     */
    public static final String LOGIN_USER = "LOGIN_USER";

    /**
     * 用户名
     */
    public static final String USER_NAME = "USER_NAME";

    /**
     * 时间格式
     */
    public static final String FORMAT = "yyyy-MM-dd";

    /**
     * 半天，在redis中最长缓存时间
     */
    public static final int RECORD_CACHE_TIME = 60 * 60 * 12;

    /**
     * 一分钟
     */
    public static final int ONE_MINUTE = 60;

    /**
     * 一刻钟
     */
    public static final int QUARTER = 60 * 15;

    /**
     * 当前项目的工作路径
     */
    public static final String BASE_PATH = System.getProperty("user.dir");

    /**
     * 文件路径
     */
    public static final String DOCUMENT_PATH = BASE_PATH + "/file/documents/";

    /**
     * 视频路径
     */
    public static final String VIDEO_PATH = BASE_PATH + "/file/videos/";

    /**
     * 图像路径
     */
    public static final String IMAGE_PATH = BASE_PATH + "/file/images/";

    /**
     * 视频帧等的路径
     */
    public static final String MATERIAL_PATH = BASE_PATH + "/file/material/";

    /**
     * 音频路径
     */
    public static final String AUDIO_PATH = BASE_PATH + "/file/audios/";

    /**
     * 路径分隔符
     */
    public static final String SEPARATOR = File.separator;

    /**
     * 一天
     */
    public static final int ONE_DAY = 60 * 60 * 24;

    /**
     * 普通用户
     */
    public static final int USER = 0;

    /**
     * 管理员
     */
    public static final int ADMIN = 1;

    /**
     * 未审批
     */
    public static final int NOT_APPROVAL = 0;

    /**
     * 同意
     */
    public static final int AGREE = 1;

    /**
     * 拒绝
     */
    public static final int REFUSE = 2;
}

