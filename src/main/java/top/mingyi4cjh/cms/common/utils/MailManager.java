package top.mingyi4cjh.cms.common.utils;

import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:49
 */
@Component
public class MailManager {

    /**
     * 163邮箱的授权码
     */
    private final String AUTH_CODE = "JNYZPJMORLBETXZB";

    private final String SOURCE_EMAIL = "mingyi4cjh@163.com";

    private final String SOURCE_NAME = "MingYI";

    private static final Pattern MAIL_PATTERN = Pattern.compile("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");

    public boolean checkEmail(String email) {
        Matcher matcher = MAIL_PATTERN.matcher(email);
        return matcher.matches();
    }

    /**
     * 发送otpCode到对应的address
     *
     * @param address 目的地址
     * @param code    验证码
     */
    @Async("Executor")
    public void sendOtpCode(String address, String code) {
        HtmlEmail email = new HtmlEmail();
        Logger logger = LoggerFactory.getLogger(this.getClass());

        try {
            email.setHostName("smtp.163.com");
            email.setCharset("utf-8");
            email.setFrom(SOURCE_EMAIL, SOURCE_NAME);
            email.setAuthentication(SOURCE_EMAIL, AUTH_CODE);
            email.setSubject("邮箱验证");
            email.addTo(address);
            email.setHtmlMsg("<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<p>尊敬的用户：</p>\n" +
                    "<p>您好！</p>\n" +
                    "\n" +
                    "<p>感谢您注册成为本站用户，您的验证码为：</p>\n" +
                    "<b>" + code +
                    "</b>\n" +
                    "<p>请于15分钟内完成注册，祝您使用愉快！</p>\n" +
                    "\n" +
                    "<HR align=\"left\" width=300 color=gray SIZE=1>\n" +
                    "<p style=\"color:gray\">此邮件为系统发送，请勿直接回复。</p>\n" +
                    "<p style=\"color:gray\">如非本人操作，请忽略此邮件，对您造成的打扰，我们深表歉意。</p>\n" +
                    "\n" +
                    "<br>\n" +
                    "<br>\n" +
                    "MingYI\n" +
                    "</body>\n" +
                    "</html>");

            email.setTextMsg("尊敬的用户：\r\n" +
                    "您好！\r\n" +
                    "\r\n" +
                    "感谢您注册本网站的内容管理系统服务，您的验证码为：\r\n" +
                    code + "\r\n" +
                    "请于15分钟内完成注册，祝您使用愉快！\r\n" +
                    "\r\n" +
                    "此邮件为系统发送，请勿直接回复。\r\n" +
                    "如非本人操作，请忽略此邮件，对您造成的打扰，我们深表歉意。\r\n" +
                    "\r\n" +
                    "\r\n" +
                    "MingYI");

            email.send();
            logger.info("Mail send to {} successful.", address);
        } catch (Exception e) {
            logger.error("Send Mail To {} Error, Error Info:", address, e);
        }
    }

    /**
     * 向对应的address发送新的otpCode，与sendOtpCode方法不同于内容模板不同
     *
     * @param address 目的地址
     * @param code    验证码
     */
    @Async("Executor")
    public void resendOtpCode(String address, String code) {
        HtmlEmail email = new HtmlEmail();
        Logger logger = LoggerFactory.getLogger(this.getClass());

        try {
            email.setHostName("smtp.163.com");
            email.setCharset("utf-8");
            email.setFrom(SOURCE_EMAIL, SOURCE_NAME);
            email.setAuthentication(SOURCE_EMAIL, AUTH_CODE);
            email.setSubject("重置密码");
            email.addTo(address);

            email.setHtmlMsg("<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<p>尊敬的用户：</p>\n" +
                    "<p>您好！</p>\n" +
                    "\n" +
                    "<p>您正在重置您的密码，您的验证码为：</p>\n" +
                    "<b>" + code +
                    "</b>\n" +
                    "<p>请于15分钟内完成密码重置，祝您使用愉快！</p>\n" +
                    "\n" +
                    "<HR align=\"left\" width=300 color=gray SIZE=1>\n" +
                    "<p style=\"color:gray\">此邮件为系统发送，请勿直接回复。</p>\n" +
                    "<p style=\"color:gray\">如非本人操作，请忽略此邮件，对您造成的打扰，我们深表歉意。</p>\n" +
                    "\n" +
                    "<br>\n" +
                    "<br>\n" +
                    "MingYI\n" +
                    "</body>\n" +
                    "</html>");

            email.setTextMsg("尊敬的用户：\r\n" +
                    "您好！\r\n" +
                    "\r\n" +
                    "您正在重置您的密码，您的验证码为：\r\n" +
                    code + "\r\n" +
                    "请于15分钟内完成密码重置，祝您使用愉快！\r\n" +
                    "\r\n" +
                    "此邮件为系统发送，请勿直接回复。\r\n" +
                    "如非本人操作，请忽略此邮件，对您造成的打扰，我们深表歉意。\r\n" +
                    "\r\n" +
                    "\r\n" +
                    "MingYI");

            email.send();
            logger.info("Mail send to {} successful.", address);
        } catch (Exception e) {
            logger.error("Resend Mail To {} Error, Error Info:", address, e);
        }
    }
}

