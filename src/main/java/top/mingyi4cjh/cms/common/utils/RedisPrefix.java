package top.mingyi4cjh.cms.common.utils;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:48
 */
public enum RedisPrefix {
    /**
     * 存储email_otpCode的redis的key的前缀
     */
    MAIL_OTP("cms:email:"),
    /**
     * userName-email键值对，用于验证userName和email的关系
     */
    NAME_MAIL("cms:n-e:"),
    /**
     * email-userName键值对，用于验证userName和email的关系
     */
    MAIL_NAME("cms:e-n:"),
    /**
     * id-email键值对，用于快速根据id获取email
     */
    ID_MAIL("cms:i-e:"),
    /**
     * id-userName键值对，用于快速根据id获取userName
     */
    ID_NAME("cms:i-n:"),

    /**
     * 检查email的ttl使用，用于防止频繁请求otp
     */
    MAIL_TTL("cms:ttl:email:"),

    /**
     * 正在进行重置密码的user
     */
    RESET_PASSWORD("cms:reset:userId:"),

    /**
     * fileInfo记录的缓存
     */
    FILE_INFO_RECORD("cms:file:fileId:"),

    /**
     * 一个userId对应的文件列表
     */
    USER_FILE_LIST("cms:file:userId:"),

    /**
     * 用户信息
     */
    USER_INFO("cms:user:info:"),
    ;

    private final String prefix;

    RedisPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}

