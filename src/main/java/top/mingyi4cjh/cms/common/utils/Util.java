package top.mingyi4cjh.cms.common.utils;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:45
 */
public class Util {
    public static String replaceStr(String str) {
        return str.replaceAll("\n", "").replaceAll("\r", "");
    }
}
