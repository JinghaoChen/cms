package top.mingyi4cjh.cms.common.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import top.mingyi4cjh.cms.common.error.BusinessException;
import top.mingyi4cjh.cms.common.error.EmBusinessError;

import java.util.HashSet;
import java.util.Set;

import static top.mingyi4cjh.cms.common.utils.Field.*;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/27 10:53
 */
@Component
public class FilePathFactory implements InitializingBean {
    private Set<String> videoSet = new HashSet<>();

    private Set<String> documentSet = new HashSet<>();

    private Set<String> imageSet = new HashSet<>();

    private Set<String> audioSet = new HashSet<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        videoSet.add("video/mp4");

        documentSet.add("application/pdf");
        documentSet.add("text/plain");
        documentSet.add("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        documentSet.add("application/msword");
        documentSet.add("application/vnd.ms-excel");
        documentSet.add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        documentSet.add("application/vnd.openxmlformats-officedocument.presentationml.presentation");
        documentSet.add("application/vnd.ms-powerpoint");

        imageSet.add("image/png");
        imageSet.add("image/jpeg");

        audioSet.add("audio/mpeg");
    }

    public String getFilePathByType(String fileType) {
        if (documentSet.contains(fileType)) {
            return DOCUMENT_PATH;
        } else if (imageSet.contains(fileType)) {
            return IMAGE_PATH;
        } else if (videoSet.contains(fileType)) {
            return VIDEO_PATH;
        } else if (audioSet.contains(fileType)) {
            return AUDIO_PATH;
        } else {
            throw new BusinessException(EmBusinessError.UNSUPPORTED_FILE_TYPE);
        }
    }

    public boolean isCanConvert(String fileType) {
        if (documentSet.contains(fileType) || imageSet.contains(fileType) || videoSet.contains(fileType) || audioSet.contains(fileType)) {
            return true;
        }
        return false;
    }

}
