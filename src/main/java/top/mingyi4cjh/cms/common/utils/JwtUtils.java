package top.mingyi4cjh.cms.common.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Map;

import static top.mingyi4cjh.cms.common.utils.Field.ONE_DAY;


/**
 * @author MingYi
 */
@Component
@PropertySource("classpath:mingyi4cjh.properties")
public class JwtUtils {

    private static String SIGNATURE;

    @Value("${top.mingyi4cjh.jwt.signature}")
    private void setSignature(String signature) {
        JwtUtils.SIGNATURE = signature;
    }

    /**
     * 生成token，放在payload中的数据可以通过map方式传输，Utils的通用性会比较高，调用的时候需要多写一点
     * SignatureVerificationException 无效签名Exception
     * TokenExpiredException token过期Exception
     * AlgorithmMismatchException token算法不一致Exception
     *
     * @param map 参数列表
     * @return token
     */
    public static String getToken(Map<String, String> map) {
        JWTCreator.Builder builder = JWT.create();
        if (map != null && map.size() != 0){
            map.forEach((k, v) -> {
                builder.withClaim(k, v);
            });
        }


        Calendar instance = Calendar.getInstance();
        //设定1天的token有效期
        instance.add(Calendar.SECOND, ONE_DAY);
        String token = builder.withExpiresAt(instance.getTime())
                .sign(Algorithm.HMAC256(SIGNATURE));

        return token;
    }

    /**
     * 验证token的合法性
     * 这里面会抛出各种异常，需要外部，如拦截器等进行捕获、处理
     *
     * @param token token
     * @return 解析过的token信息
     */
    public static DecodedJWT verify(String token) {
        return JWT.require(Algorithm.HMAC256(SIGNATURE)).build().verify(token);
    }


}