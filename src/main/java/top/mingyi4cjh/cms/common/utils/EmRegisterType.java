package top.mingyi4cjh.cms.common.utils;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:51
 */
public enum EmRegisterType {
    /**
     * 根据邮箱注册
     */
    EMAIL("email", 10001),

    /**
     * 根据手机号注册
     */
    PHONE("phone", 10002),
    ;

    private final String typeName;
    private final Integer typeCode;

    EmRegisterType(String typeName, Integer typeCode) {
        this.typeName = typeName;
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public Integer getTypeCode() {
        return typeCode;
    }
}
