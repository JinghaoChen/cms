package top.mingyi4cjh.cms.viewobject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/27 15:16
 */
@ApiModel(description = "文件信息实体")
public class FileVO {
    @ApiModelProperty(value = "文件id", dataType = "String")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fileId;

    @ApiModelProperty(value = "文件名", dataType = "String")
    private String fileName;

    @ApiModelProperty(value = "上传时间", dataType = "Date")
    private Date uploadTime;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    @Override
    public String toString() {
        return "FileVO{" +
                "fileId=" + fileId +
                ", fileName='" + fileName + '\'' +
                ", uploadTime=" + uploadTime +
                '}';
    }
}
