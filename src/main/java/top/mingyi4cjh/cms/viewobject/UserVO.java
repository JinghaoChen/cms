package top.mingyi4cjh.cms.viewobject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/26 10:16
 */
@ApiModel(description = "用户信息实体")
public class UserVO {
    @ApiModelProperty(value = "用户id", dataType = "String")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "性别", dataType = "Byte")
    private Byte gender;

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    private Integer age;

    @ApiModelProperty(value = "注册时间", dataType = "Date")
    private Date loginDate;

    @ApiModelProperty(value = "最近登录ip", dataType = "String")
    private String ip;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                ", loginDate=" + loginDate +
                ", ip='" + ip + '\'' +
                '}';
    }
}

