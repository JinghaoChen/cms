package top.mingyi4cjh.cms.viewobject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/30 18:25
 */
@ApiModel(description = "文章信息实体")
public class ArticleVO {
    @ApiModelProperty(value = "文章id", dataType = "String")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long articleId;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "作者名", dataType = "String")
    private String authorName;

    @ApiModelProperty(value = "文章内容", dataType = "String")
    private String content;

    @ApiModelProperty(value = "阅读数", dataType = "Long")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long readingNumber;

    @ApiModelProperty(value = "文章专栏名称", dataType = "String")
    private String columnName;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date uploadTime;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getReadingNumber() {
        return readingNumber;
    }

    public void setReadingNumber(Long readingNumber) {
        this.readingNumber = readingNumber;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    @Override
    public String toString() {
        return "ArticleVO{" +
                "articleId=" + articleId +
                ", title='" + title + '\'' +
                ", authorName='" + authorName + '\'' +
                ", content='" + content + '\'' +
                ", readingNumber=" + readingNumber +
                ", columnName='" + columnName + '\'' +
                ", uploadTime=" + uploadTime +
                '}';
    }
}
