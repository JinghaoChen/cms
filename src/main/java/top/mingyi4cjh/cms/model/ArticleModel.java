package top.mingyi4cjh.cms.model;

import java.util.Date;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/30 18:31
 */
public class ArticleModel {
    private Long articleId;

    private String title;

    private Long authorId;

    private String authorName;

    private String content;

    private Long readingNumber;

    private Long columnId;

    private String columnName;

    private Date uploadTime;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getReadingNumber() {
        return readingNumber;
    }

    public void setReadingNumber(Long readingNumber) {
        this.readingNumber = readingNumber;
    }

    public Long getColumnId() {
        return columnId;
    }

    public void setColumnId(Long columnId) {
        this.columnId = columnId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    @Override
    public String toString() {
        return "ArticleModel{" +
                "articleId=" + articleId +
                ", title='" + title + '\'' +
                ", authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", content='" + content + '\'' +
                ", readingNumber=" + readingNumber +
                ", columnId=" + columnId +
                ", columnName='" + columnName + '\'' +
                ", uploadTime=" + uploadTime +
                '}';
    }
}
