package top.mingyi4cjh.cms.model;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:56
 */
public class UserLoginModel {
    private Integer id;

    @NotNull(message = "用户id不能为空")
    private Long userId;

    private Date loginDate;

    @NotNull(message = "登录ip不能为空")
    private String ip;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "UserLoginModel{" +
                "id=" + id +
                ", userId=" + userId +
                ", loginDate=" + loginDate +
                ", ip='" + ip + '\'' +
                '}';
    }
}

