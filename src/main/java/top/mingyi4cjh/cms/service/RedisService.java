package top.mingyi4cjh.cms.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/21 23:58
 */
public interface RedisService {

    /**
     * 保存属性
     *
     * @param key   key
     * @param value value
     * @param time  ttl
     */
    void set(String key, Object value, long time);

    /**
     * 保存属性
     *
     * @param key   key
     * @param value value
     */
    void set(String key, Object value);

    /**
     * 获取属性
     *
     * @param key key
     * @return value
     */
    Object get(String key);

    /**
     * 删除属性
     *
     * @param key key
     * @return null when used in pipeline / transaction.
     */
    Boolean del(String key);

    /**
     * 批量删除属性
     *
     * @param keys keys
     * @return null when used in pipeline / transaction.
     */
    Long del(List<String> keys);

    /**
     * 设置过期时间
     *
     * @param key  key
     * @param time ttl
     * @return ttl设置状态
     */
    Boolean expire(String key, long time);


    /**
     * 获取过期时间
     *
     * @param key key
     * @return null when used in pipeline / transaction.
     */
    Long getExpire(String key);

    /**
     * 判断是否有该属性
     *
     * @param key key
     * @return true/false
     */
    Boolean hasKey(String key);

    /**
     * 按delta递增
     *
     * @param key   key
     * @param delta delta
     * @return null when used in pipeline / transaction.
     */
    Long incr(String key, long delta);

    /**
     * 按delta递减
     *
     * @param key   key
     * @param delta delta
     * @return null when used in pipeline / transaction.
     */
    Long decr(String key, long delta);

    /**
     * n
     * 获取Hash结构中的属性
     *
     * @param key     key
     * @param hashKey field
     * @return null when key or hashKey does not exist or used in pipeline / transaction.
     */
    Object hGet(String key, String hashKey);

    /**
     * 向Hash结构中放入一个属性
     *
     * @param key     key
     * @param hashKey field
     * @param value   value
     * @param time    ttl
     * @return true/false
     */
    Boolean hSet(String key, String hashKey, Object value, long time);

    /**
     * 向Hash结构中放入一个属性
     *
     * @param key     key
     * @param hashKey field
     * @param value   value
     */
    void hSet(String key, String hashKey, Object value);

    /**
     * 直接获取整个Hash结构
     *
     * @param key key
     * @return hash中的所有数据
     */
    Map<Object, Object> hGetAll(String key);

    /**
     * 直接设置整个Hash结构
     *
     * @param key  key
     * @param map  field and value
     * @param time ttl
     * @return ttl设置状态
     */
    Boolean hSetAll(String key, Map<String, Object> map, long time);

    /**
     * 直接设置整个Hash结构
     *
     * @param key key
     * @param map field and value
     */
    void hSetAll(String key, Map<String, Object> map);

    /**
     * 删除Hash结构中的属性
     *
     * @param key     key
     * @param hashKey field
     */
    void hDel(String key, Object... hashKey);

    /**
     * 判断Hash结构中是否有该属性
     *
     * @param key     key
     * @param hashKey field
     * @return null when used in pipeline / transaction.
     */
    Boolean hHasKey(String key, String hashKey);

    /**
     * Hash结构中属性递增
     *
     * @param key     key
     * @param hashKey field
     * @param delta   delta
     * @return null when used in pipeline / transaction.
     */
    Long hIncr(String key, String hashKey, Long delta);

    /**
     * Hash结构中属性递减
     *
     * @param key     key
     * @param hashKey field
     * @param delta   delta
     * @return null when used in pipeline / transaction.
     */
    Long hDecr(String key, String hashKey, Long delta);

    /**
     * 获取Set结构
     *
     * @param key key
     * @return null when used in pipeline / transaction.
     */
    Set<Object> sMembers(String key);

    /**
     * 向Set结构中添加属性
     *
     * @param key    key
     * @param values values
     * @return null when used in pipeline / transaction.
     */
    Long sAdd(String key, Object... values);

    /**
     * 向Set结构中添加属性
     *
     * @param key    key
     * @param time   ttl
     * @param values values
     * @return null when used in pipeline / transaction.
     */
    Long sAdd(String key, long time, Object... values);

    /**
     * 向set中插入一个元素
     *
     * @param key   key
     * @param value value
     * @return null when used in pipeline / transaction.
     */
    Long sAddOne(String key, Object value);

    /**
     * 向Set中插入一个元素，并增加ttl
     *
     * @param key   key
     * @param time  ttl
     * @param value value
     * @return null when used in pipeline / transaction.
     */
    Long sAddOne(String key, long time, Object value);

    /**
     * 是否为Set中的属性
     *
     * @param key   key
     * @param value value
     * @return null when used in pipeline / transaction.
     */
    Boolean sIsMember(String key, Object value);

    /**
     * 获取Set结构的长度
     *
     * @param key key
     * @return null when used in pipeline / transaction.
     */
    Long sSize(String key);

    /**
     * 删除Set结构中的属性
     *
     * @param key    key
     * @param values values
     * @return null when used in pipeline / transaction.
     */
    Long sRemove(String key, Object... values);

    /**
     * 获取List结构中的属性
     *
     * @param key   key
     * @param start 起始index
     * @param end   结束index
     * @return null when used in pipeline / transaction.
     */
    List<Object> lRange(String key, long start, long end);

    /**
     * 获取List结构的长度
     *
     * @param key key
     * @return null when used in pipeline / transaction.
     */
    Long lSize(String key);

    /**
     * 根据索引获取List中的属性
     *
     * @param key   key
     * @param index index
     * @return null when used in pipeline / transaction.
     */
    Object lIndex(String key, long index);

    /**
     * 向List结构中添加属性
     *
     * @param key   key
     * @param value value
     * @return null when used in pipeline / transaction.
     */
    Long lPush(String key, Object value);

    /**
     * 向List结构中添加属性
     *
     * @param key   key
     * @param value value
     * @param time  过期时间
     * @return null when used in pipeline / transaction.
     */
    Long lPush(String key, Object value, long time);

    /**
     * 向List结构中批量添加属性
     *
     * @param key    key
     * @param values values
     * @return null when used in pipeline / transaction.
     */
    Long lPushAll(String key, Object... values);

    /**
     * 向List结构中批量添加属性
     *
     * @param key    key
     * @param time   过期时间
     * @param values values
     * @return null when used in pipeline / transaction.
     */
    Long lPushAll(String key, Long time, Object... values);

    /**
     * 从List结构中移除属性
     *
     * @param key   key
     * @param count 移除个数
     * @param value value
     * @return null when used in pipeline / transaction.
     */
    Long lRemove(String key, long count, Object value);
}

