package top.mingyi4cjh.cms.service;

import top.mingyi4cjh.cms.dataobject.ArticleApprovalDO;
import top.mingyi4cjh.cms.model.ArticleModel;

import java.util.List;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/30 18:22
 */
public interface ArticleService {
    /**
     * 上传一个文章
     *
     * @param articleModel 文章信息
     */
    void createArticle(ArticleModel articleModel);

    /**
     * 删除文章
     *
     * @param articleId 文章id
     */
    void deleteArticle(Long articleId);

    /**
     * 获取文章信息
     *
     * @param articleId 文章id
     * @return 文章信息
     */
    ArticleModel getArticle(Long articleId);

    /**
     * 获取用户自己写过的文章列表
     *
     * @param authorId 作者id
     * @return 文章列表
     */
    List<ArticleModel> getSelfList(Long authorId);

    /**
     * 校验该用户是否是文章作者
     *
     * @param userId    用户id
     * @param articleId 文章id
     * @return true/false
     */
    boolean isArticleOwner(Long userId, Long articleId);

    /**
     * 查询身份信息
     *
     * @param userId 用户id
     * @return 身份信息
     */
    int queryIdentify(Long userId);

    /**
     * 审批
     *
     * @param articleId 文章号
     * @param status    审批状态
     * @param userId    审批人
     */
    void approval(Long articleId, Integer status, Long userId);

    /**
     * 获取待审批列表
     *
     * @return 待审批列表
     */
    List<ArticleApprovalDO> getNotApprovalList();
}
