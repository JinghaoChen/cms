package top.mingyi4cjh.cms.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import top.mingyi4cjh.cms.dataobject.FileInfoDO;
import top.mingyi4cjh.cms.dataobject.UserInfoDO;
import top.mingyi4cjh.cms.model.FileModel;
import top.mingyi4cjh.cms.model.UserModel;

import java.util.List;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/26 23:59
 */
public interface FileService {

    /**
     * 上传一个文件
     *
     * @param fileId        文件id
     * @param multipartFile 要上传的文件
     * @param userId        上传者id
     */
    void uploadFile(Long fileId, MultipartFile multipartFile, Long userId);

    /**
     * 检查对应的fileId是否属于该用户
     *
     * @param userId 用户id
     * @param fileId 文件id
     * @return 匹配结果
     */
    boolean isFileOwner(Long userId, Long fileId);

    /**
     * 下载文件
     *
     * @param fileId 文件id
     * @return
     */
    String downFile(Long fileId);

    /**
     * 删除文件
     *
     * @param fileId
     */
    void deleteFile(Long fileId);

    /**
     * 获取一个用户的文件列表
     *
     * @param userId 用户id
     * @return 文件列表
     */
    List<FileModel> getFileList(Long userId);

    /**
     * 预览文件
     *
     * @param fileId 文件id
     * @return 文件能否预览
     */
    String previewFilePath(Long fileId);

    FileModel convertFromFileInfo(FileInfoDO userInfoDO);
}
