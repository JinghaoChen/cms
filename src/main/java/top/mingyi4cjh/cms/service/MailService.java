package top.mingyi4cjh.cms.service;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/22 00:01
 */
public interface MailService {

    /**
     * 防止用户频繁调用接口，浪费系统资源。
     * 查看一分钟内是否已经向改邮箱发送过验证码。
     * 若已经发送过，则返回false，反之为true。
     *
     * @param address 希望发送邮件的目的邮箱
     * @return true/false
     */
    boolean checkTtl(String address);

    /**
     * 检测email是否是规范的，规范则为true否则为false
     *
     * @param email 待检测的email
     * @return true / false
     */
    boolean checkEmail(String email);

    /**
     * 给对应的邮箱发送验证码。
     *
     * @param address  邮件的目的邮箱
     * @param otpCode  验证码
     * @param isResent 此次发送是否为重发送
     */
    void sendOtpCode(String address, String otpCode, boolean isResent);

    /**
     * 清理对应邮箱的限制，包括ttl和email-otpCode
     *
     * @param email 发送过邮件的目的地址
     */
    void cleanCache(String email);
}
