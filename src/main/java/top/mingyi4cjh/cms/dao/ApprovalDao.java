package top.mingyi4cjh.cms.dao;

import org.springframework.stereotype.Component;
import top.mingyi4cjh.cms.dataobject.ArticleApprovalDO;
import top.mingyi4cjh.cms.service.RedisService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author MingYi
 */
@Component
public class ApprovalDao {

    @Resource
    private ArticleApprovalDOMapper articleApprovalDOMapper;

    @Resource
    private RedisService redisService;

    public void update(Long articleId, Integer status, Long userId) {
        ArticleApprovalDO articleApprovalDO = new ArticleApprovalDO();
        articleApprovalDO.setArticleId(articleId);
        articleApprovalDO.setApprovalStatus(status);
        articleApprovalDO.setApproverId(userId);
        articleApprovalDO.setApprovalTime(new Date(System.currentTimeMillis()));
        articleApprovalDOMapper.updateByPrimaryKeySelective(articleApprovalDO);

        String key = "cms:article:" + articleId;
        redisService.del(key);
    }

    public void insert(Long articleId) {
        ArticleApprovalDO articleApprovalDO = new ArticleApprovalDO();
        articleApprovalDO.setArticleId(articleId);
        articleApprovalDOMapper.insertSelective(articleApprovalDO);
    }

    public List<ArticleApprovalDO> getNotApprovalList() {
        return articleApprovalDOMapper.queryNotApprovalList();
    }


}
