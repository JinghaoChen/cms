package top.mingyi4cjh.cms.dao;

import top.mingyi4cjh.cms.dataobject.UserInfoDO;

/**
 * @author MingYi
 * @program clock
 */
public interface UserInfoDOMapper {
    /**
     * 根据主键删除一条记录
     *
     * @param userId userId
     * @return 1/0
     */
    int deleteByPrimaryKey(Long userId);

    /**
     * 插入一条记录
     *
     * @param record 待插入的数据
     * @return 1/0
     */
    int insert(UserInfoDO record);

    /**
     * 选择性地插入一条记录
     *
     * @param record 待插入的数据
     * @return 1/0
     */
    int insertSelective(UserInfoDO record);

    /**
     * 根据主键查询一条记录
     *
     * @param userId userId
     * @return 查询结果
     */
    UserInfoDO selectByPrimaryKey(Long userId);

    /**
     * 根据userName从user_info表中查询用户信息。
     *
     * @param userName userName
     * @return 查询结果
     */
    UserInfoDO selectByUserName(String userName);

    /**
     * 根据邮箱从user_info表中查询一个结果。
     *
     * @param email email
     * @return 查询结果
     */
    UserInfoDO selectByEmail(String email);

    /**
     * 根据电话号码从user_info表中查询一个结果。
     *
     * @param phone 1/0
     * @return 查询结果
     */
    UserInfoDO selectByPhone(String phone);

    /**
     * 根据主键选择性地更新一条记录
     *
     * @param record 待更新的新数据
     * @return 1/0
     */
    int updateByPrimaryKeySelective(UserInfoDO record);

    /**
     * 根据主键更新一条记录
     *
     * @param record 待更新的新数据
     * @return 1/0
     */
    int updateByPrimaryKey(UserInfoDO record);
}