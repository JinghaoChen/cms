package top.mingyi4cjh.cms.dao;

import org.springframework.stereotype.Component;
import top.mingyi4cjh.cms.dataobject.ArticleDO;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/30 20:05
 */
@Component
public class ArticleDao {
    @Resource
    private ArticleDOMapper articleDOMapper;

    public int insert(ArticleDO record) {
        return articleDOMapper.insertSelective(record);
    }

    public ArticleDO selectByArticleId(Long articleId) {
        //todo 增加浏览量,可以考虑用redis的incr，然后一段时间写入一次数据库

        //todo 这里可以加缓存
        return articleDOMapper.selectByPrimaryKey(articleId);
    }

    public List<ArticleDO> selectByAuthorId(Long authorId) {
        return articleDOMapper.selectByAuthorId(authorId);
    }

    public int updateByPrimaryKey(ArticleDO record) {
        return articleDOMapper.updateByPrimaryKeySelective(record);
    }

    public int deleteByArticleId(Long articleId) {
        ArticleDO record = new ArticleDO();
        record.setDeleted(true);
        record.setArticleId(articleId);
        return articleDOMapper.updateByPrimaryKeySelective(record);
    }

}
