package top.mingyi4cjh.cms.dao;

import top.mingyi4cjh.cms.dataobject.UserPasswordDO;

/**
 * @author MingYi
 * @program clock
 */
public interface UserPasswordDOMapper {
    /**
     * 根据主键删除一条记录
     *
     * @param userId userId
     * @return 1/0
     */
    int deleteByPrimaryKey(Long userId);

    /**
     * 插入一条记录
     *
     * @param record 待插入的数据
     * @return 1/0
     */
    int insert(UserPasswordDO record);

    /**
     * 选择性地插入一条记录
     *
     * @param record 待插入的数据
     * @return 1/0
     */
    int insertSelective(UserPasswordDO record);

    /**
     * 根据主键查询记录
     *
     * @param userId userId
     * @return 查询结果
     */
    UserPasswordDO selectByPrimaryKey(Long userId);

    /**
     * 根据主键选择性地更新一条记录
     *
     * @param record 待更新的新数据
     * @return 1/0
     */
    int updateByPrimaryKeySelective(UserPasswordDO record);

    /**
     * 根据主键更新一条记录
     *
     * @param record 待更新的新数据
     * @return 1/0
     */
    int updateByPrimaryKey(UserPasswordDO record);
}