package top.mingyi4cjh.cms.dao;

import org.springframework.stereotype.Component;
import top.mingyi4cjh.cms.dataobject.ArticleColumnDO;

import javax.annotation.Resource;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/30 20:09
 */
@Component
public class ArticleColumnDao {
    @Resource
    private ArticleColumnDOMapper articleColumnDOMapper;

    public int insert(ArticleColumnDO record) {
        return articleColumnDOMapper.insertSelective(record);
    }

    public ArticleColumnDO selectByColumnId(Long columnId) {
        //todo 这里可以加缓存
        return articleColumnDOMapper.selectByPrimaryKey(columnId);
    }

    public int updateByArticleId(ArticleColumnDO record) {
        return articleColumnDOMapper.updateByPrimaryKeySelective(record);
    }

    public int deleteByPrimaryKey(Long columnId) {
        ArticleColumnDO record = new ArticleColumnDO();
        record.setColumnId(columnId);
        record.setDeleted(true);
        return articleColumnDOMapper.updateByPrimaryKeySelective(record);
    }
}
