package top.mingyi4cjh.cms.dao;


import top.mingyi4cjh.cms.dataobject.RegisterTypeDO;

/**
 * @author MingYi
 * @program clock
 */
public interface RegisterTypeDOMapper {

    /**
     * 根据主键删除一条记录
     *
     * @param registerTypeId 注册方式号
     * @return 1/0
     */
    int deleteByPrimaryKey(Integer registerTypeId);

    /**
     * 插入一条记录
     *
     * @param record 待插入的信息
     * @return 1/0
     */
    int insert(RegisterTypeDO record);

    /**
     * 选择性地插入一条记录
     *
     * @param record 待插入的信息
     * @return 1/0
     */
    int insertSelective(RegisterTypeDO record);

    /**
     * 根据主键查询一条记录
     *
     * @param registerTypeId 注册方式号
     * @return 查询结果
     */
    RegisterTypeDO selectByPrimaryKey(Integer registerTypeId);

    /**
     * 根据主键选择性地更新一条记录
     *
     * @param record 待更新的新数据
     * @return 1/0
     */
    int updateByPrimaryKeySelective(RegisterTypeDO record);

    /**
     * 根据主键更新一条记录
     *
     * @param record 待更新的新数据
     * @return 1/0
     */
    int updateByPrimaryKey(RegisterTypeDO record);
}