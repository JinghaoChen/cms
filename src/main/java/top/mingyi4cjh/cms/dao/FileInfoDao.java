package top.mingyi4cjh.cms.dao;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import top.mingyi4cjh.cms.dataobject.FileInfoDO;
import top.mingyi4cjh.cms.service.RedisService;

import javax.annotation.Resource;
import java.util.List;

import static top.mingyi4cjh.cms.common.utils.Field.QUARTER;
import static top.mingyi4cjh.cms.common.utils.RedisPrefix.FILE_INFO_RECORD;

/**
 * @author MingYi
 * @program cms
 * @create 2022/04/27 00:32
 */
@Component
public class FileInfoDao {

    @Resource
    private RedisService redisService;

    @Resource
    private FileInfoDOMapper fileInfoDOMapper;

    public int insert(FileInfoDO record) {
        return fileInfoDOMapper.insertSelective(record);
    }

    public FileInfoDO selectByPrimaryKey(Long fileId) {
        FileInfoDO result;
        String key = FILE_INFO_RECORD + String.valueOf(fileId);
        String resultJson = (String) redisService.get(key);
        if (!StringUtils.isEmpty(resultJson)) {
            redisService.expire(key, QUARTER);
            result = JSON.parseObject(resultJson, FileInfoDO.class);
            return result;
        }

        result = fileInfoDOMapper.selectByPrimaryKey(fileId);
        String record = JSON.toJSONString(result);
        redisService.set(key, record, QUARTER);
        return result;
    }

    public List<FileInfoDO> selectByUserId(Long userId) {
//        List<FileInfoDO> result;
//        String key = USER_FILE_LIST + String.valueOf(userId);
//        if (redisService.hasKey(key)) {
//            List tempResult = redisService.lRange(key, 1, redisService.lSize(key));
//            redisService.expire(key, QUARTER);
//            result = convertObjectToDO(tempResult);
//            return result;
//        }
//
//        result = fileInfoDOMapper.selectByUserId(userId);
//        redisService.lPushAll(key,QUARTER, result.stream().map(this::convertDoToJson).collect(Collectors.toList()));
//        return result;
        return fileInfoDOMapper.selectByUserId(userId);
    }

    public int updateByPrimaryKey(FileInfoDO record) {
        return fileInfoDOMapper.updateByPrimaryKeySelective(record);
    }

    public int deleteByPrimaryKey(Long fileId) {
        FileInfoDO record = new FileInfoDO();
        record.setFileId(fileId);
        record.setDeleted(true);
        if (redisService.hasKey(FILE_INFO_RECORD + String.valueOf(fileId))) {
            redisService.del(FILE_INFO_RECORD + String.valueOf(fileId));
        }
        return fileInfoDOMapper.updateByPrimaryKeySelective(record);
    }

    private String convertDoToJson(FileInfoDO fileInfoDO) {
        return JSON.toJSONString(fileInfoDO);
    }

//    private List<FileInfoDO> convertObjectToDO(List list) {
//        if (list == null) {
//            return null;
//        }
//
////        String temp = (String) o;
////        return JSON.parseObject(temp, FileInfoDO.class);
//
//        List<FileInfoDO> result = new ArrayList<>();
//        for (Object o : list) {
//            result.add(JSON.parseObject((String) o, FileInfoDO.class)); //CCE
//        }
//
//        return result;
//    }
}
