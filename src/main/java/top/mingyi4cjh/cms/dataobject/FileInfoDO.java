package top.mingyi4cjh.cms.dataobject;

import java.util.Date;

public class FileInfoDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.file_id
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private Long fileId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.file_name
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private String fileName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.file_type
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private String fileType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.upload_time
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private Date uploadTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.file_size
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private Long fileSize;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.file_url
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private String fileUrl;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.user_id
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private Long userId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_info.deleted
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    private Boolean deleted;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.file_id
     *
     * @return the value of file_info.file_id
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public Long getFileId() {
        return fileId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.file_id
     *
     * @param fileId the value for file_info.file_id
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.file_name
     *
     * @return the value of file_info.file_name
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.file_name
     *
     * @param fileName the value for file_info.file_name
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.file_type
     *
     * @return the value of file_info.file_type
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.file_type
     *
     * @param fileType the value for file_info.file_type
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setFileType(String fileType) {
        this.fileType = fileType == null ? null : fileType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.upload_time
     *
     * @return the value of file_info.upload_time
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public Date getUploadTime() {
        return uploadTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.upload_time
     *
     * @param uploadTime the value for file_info.upload_time
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.file_size
     *
     * @return the value of file_info.file_size
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public Long getFileSize() {
        return fileSize;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.file_size
     *
     * @param fileSize the value for file_info.file_size
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.file_url
     *
     * @return the value of file_info.file_url
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public String getFileUrl() {
        return fileUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.file_url
     *
     * @param fileUrl the value for file_info.file_url
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.user_id
     *
     * @return the value of file_info.user_id
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.user_id
     *
     * @param userId the value for file_info.user_id
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_info.deleted
     *
     * @return the value of file_info.deleted
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_info.deleted
     *
     * @param deleted the value for file_info.deleted
     *
     * @mbg.generated Wed Apr 27 11:17:19 CST 2022
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}