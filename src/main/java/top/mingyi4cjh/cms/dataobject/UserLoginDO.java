package top.mingyi4cjh.cms.dataobject;

import java.util.Date;

public class UserLoginDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.id
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.user_id
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    private Long userId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.login_date
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    private Date loginDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.ip
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    private String ip;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.id
     *
     * @return the value of user_login.id
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.id
     *
     * @param id the value for user_login.id
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.user_id
     *
     * @return the value of user_login.user_id
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.user_id
     *
     * @param userId the value for user_login.user_id
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.login_date
     *
     * @return the value of user_login.login_date
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public Date getLoginDate() {
        return loginDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.login_date
     *
     * @param loginDate the value for user_login.login_date
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.ip
     *
     * @return the value of user_login.ip
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public String getIp() {
        return ip;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.ip
     *
     * @param ip the value for user_login.ip
     *
     * @mbg.generated Mon Apr 25 21:10:22 CST 2022
     */
    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }
}