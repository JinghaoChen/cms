package top.mingyi4cjh.cms;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: File-recommend
 * @author: MingYi
 * @create: 2022/04/05 22:44:54
 */
@RestController("test")
@RequestMapping("/")
public class TestController {

    @GetMapping("hello")
    public String sayHello() {
        return "Hello";
    }

    @GetMapping("/test/{id}/1")
    public String testHello(@PathVariable String id) {
        return id;
    }
}
